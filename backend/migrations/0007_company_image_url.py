# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_mypointsuser_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='image_url',
            field=models.URLField(blank=True),
        ),
    ]
