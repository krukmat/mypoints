# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0022_auto_20180924_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='image',
            field=models.FileField(default=b'', upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='reward',
            name='image_url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='last_checkout_awards',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 20, 19, 29, 42, 939923)),
        ),
    ]