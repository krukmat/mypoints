# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_socialaccount_social'),
    ]

    operations = [
        migrations.AddField(
            model_name='mypointsuser',
            name='state',
            field=models.CharField(max_length=255, null=True, verbose_name='state'),
        ),
    ]
