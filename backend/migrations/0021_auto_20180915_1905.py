# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0020_subscription_shared_rewards'),
    ]

    operations = [
        migrations.AddField(
            model_name='reward',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 15, 19, 5, 9, 50161), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subscription',
            name='last_checkout_awards',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 15, 19, 4, 26, 847486)),
        ),
    ]