# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0021_auto_20180915_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='reward',
            name='image',
            field=models.FileField(default=b'', upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='last_checkout_awards',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 24, 15, 41, 22, 55143)),
        ),
    ]