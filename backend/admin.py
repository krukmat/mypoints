from django.contrib import admin
from models import *
from forms import *


class SubscriptionAdmin(admin.ModelAdmin):
    form = SubscriptionForm

class CompanyAdmin(admin.ModelAdmin):
    pass
    #exclude = ['user']

    #def save_model(self, request, obj, form, change):
        #obj.user = request.user  # no need to check for it.
        #obj.save()


class CategoryAdmin(admin.ModelAdmin):
    exclude = ['user']


class RewardAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'type', 'stock', 'period_from', 'period_to')
    fields = ('company', 'name', 'description', 'type', 'stock', 'period_from', 'period_to',
              'terms', 'points', 'active', 'image_url', 'image')
    exclude = ['user', 'currency', 'codigo']

class RewardxUserAdmin(admin.ModelAdmin):
    list_display = ('code',)


class MyPointsUserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')
    form = MyPointsUserForm

    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'subscriptions', 'is_company', 'email')}),
    )

    exclude = ['objects', 'groups', 'user_permissions', 'date_joined', 'last_login',
               'address', 'state', 'country', 'city', 'phone', 'password']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Reward, RewardAdmin)
admin.site.register(MyPointsUser, MyPointsUserAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(RewardxUser, RewardxUserAdmin)