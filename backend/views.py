# Create your views here.

from rest_framework import serializers, viewsets
from backend.models import Company, Category, Reward, MyPointsUser, SocialAccount, Subscription, RewardxUser
from rest_framework import filters
from address.models import Address
import django_filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser
import base64
import time
import hashlib
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.generic import TemplateView, RedirectView
import requests

class CompanyFilter(filters.FilterSet):
    name_contains = django_filters.CharFilter(name='name', lookup_type='icontains')
    category_contains = django_filters.CharFilter(name='category__description', lookup_type='icontains')

    class Meta:
        model = Company
        filter_backends = (filters.DjangoFilterBackend,)
        fields = ('name_contains', 'category_contains')


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('raw',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('description',)


class RewardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reward
        fields = ('pk', 'company', 'points', 'stock', 'period_from', 'period_to', 'active',
                  'terms', 'type', 'name', 'description', 'image_url', 'real_image_url', 'image', 'company_name', 'company_image_url', 'company_category')

class CompanyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyPointsUser
        fields = ('pk', 'email',
                  'first_name', 'last_name', 'address', 'city', 'country', 'state', 'phone')


class CompanyRewardsXUserSerializer(serializers.ModelSerializer):
    reward = RewardSerializer()
    user = CompanyUserSerializer()
    class Meta:
        model = RewardxUser
        fields = ('reward', 'code', 'pending', 'status', 'user')


class CompanySerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    address = AddressSerializer()
    rewards = RewardSerializer(many=True, source='available_rewards')
    subscriptions = serializers.SerializerMethodField()
    pending_transactions = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'name', 'email', 'category', 'address', 'website',
                  'facebook', 'twitter', 'cuit', 'razon_social', 'legales', 'rewards',
                  'image_url', 'description', 'subscriptions', 'pending_transactions',
                  'share_reward_points', 'share_company_points', 'real_image_url')

    def get_subscriptions(self, obj):
        users = []
        for sub in obj.all_subscriptions():
            if sub.mypointsuser_set.all():
                users.append(sub.mypointsuser_set.all()[0])
        serializer =  CompanyUserSerializer(users, many=True)
        return serializer.data

    def get_pending_transactions(self, obj):
        rewards_pendings = RewardxUser.objects.filter(reward__company=obj)
        serializer = CompanyRewardsXUserSerializer(rewards_pendings, many=True)
        return serializer.data


class UserCompanySerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    address = AddressSerializer()
    rewards = RewardSerializer(many=True, source='available_rewards')

    class Meta:
        model = Company
        fields = ('pk', 'name', 'email', 'category', 'address', 'website',
                  'facebook', 'twitter', 'cuit', 'razon_social', 'legales', 'rewards',
                  'image_url', 'description', 'share_reward_points', 'share_company_points', 'real_image_url')

class SocialAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialAccount
        fields = ('email', 'uid', 'social')


class SubscriptionSerializer(serializers.ModelSerializer):
    company = UserCompanySerializer()
    shared_rewards = serializers.SerializerMethodField()

    class Meta:
        model = Subscription
        fields = ('points_redeemed', 'points_pending', 'company', 'share_social', 'shared_rewards')

    def get_shared_rewards(self, obj):
        serializer = RewardSerializer(obj.shared_rewards, many=True)
        return serializer.data


class RewardsXUserSerializer(serializers.ModelSerializer):
    reward = RewardSerializer()
    company = UserCompanySerializer(source='reward.company')
    class Meta:
        model = RewardxUser
        fields = ('reward', 'code', 'pending', 'company', 'status', 'user')


class EndUserSerializer(serializers.ModelSerializer):

    subscriptions = SubscriptionSerializer(many=True, read_only=True)
    social_accounts = SocialAccountSerializer(many=True, read_only=True)
    my_rewards = RewardsXUserSerializer(many=True, read_only=True)
    company_owned = serializers.SerializerMethodField()

    class Meta:
        model = MyPointsUser
        fields = ('pk', 'email', 'subscriptions',
                  'first_name', 'last_name', 'address', 'city', 'country','state',
                  'phone', 'password', 'social_accounts', 'my_rewards', 'is_company', 'company_owned')

    def get_company_owned(self, obj):
        if obj.is_company:
            company = Company.objects.filter(user=obj)
            serializer = CompanySerializer(company, many=True)
            return serializer.data
        return None



class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    filter_class = CompanyFilter
    ordering_fields = ('name',)
    ordering = ('name', )
    search_fields = ('^name',)

    def get_queryset(self):
        qs = super(CompanyViewSet, self).get_queryset()
        if self.request.user.is_company:
            return qs.filter(user=self.request.user)
        return qs

    def get_serializer_class(self):
        queryset = self.get_queryset()
        # Just in the case of a single company
        #if queryset.count() == 1:
        #    company = self.get_object()
        user = self.request.user
        if user.is_company:
            return CompanySerializer

        return UserCompanySerializer

    @detail_route(methods=['POST'])
    def update(self, request, pk=None):
        company = self.get_object()
        company.share_reward_points = request.data.get('share_reward_points')
        company.share_company_points = request.data.get('share_company_points')
        company.save()
        return Response({'status': 'OK'})

    @detail_route(methods=['put'])
    def subscribe(self, request, pk):
        subscription = Subscription(company=self.get_object())
        subscription.save()
        self.request.user.subscriptions.add(subscription)
        # TODO: Devolver el perfil para reasignarlo desde la app
        return Response({'status': 'OK'})

    @detail_route(methods=['put'])
    def share_company_points(self, request, pk):
        subscription = self.request.user.subscriptions.filter(company=self.get_object())[0]
        if not subscription.share_social:
            company = self.get_object()
            subscription.points_pending += company.share_company_points
            subscription.share_social = True
            subscription.save()
        return Response({'status': 'OK'})

    # redeem_points deberia estar en RewardViewSet
    @detail_route(methods=['post'])
    def redeem_points(self, request, pk):
        # TODO: Refactorizar. Meterlo en un metodo.
        item = request.data.get('item') #Item
        # Busco subscribe object. Menos puntos pending. Mas puntos redeemed. Guardar cambios

        # Crear registro con codigo del premio a canjear. Estado a canjear.
        reward = Reward.objects.get(pk=item['pk'])
        # Bajar del stock en 1.
        reward.stock = reward.stock - 1
        reward.save()
        millis = int(round(time.time() * 1000))
        code = hashlib.sha224(str(millis) + '_' + str(self.request.user) + '_' + str(reward.pk)).hexdigest()[:6]
        rxu = RewardxUser(reward=reward, user=self.request.user, code=code, status='PENDIENTE')

        company = Company.objects.get(pk=item['company'])
        subscription = self.request.user.subscriptions.filter(company=company)[0]
        subscription.points_pending -= item['points']
        subscription.points_redeemed += item['points']
        subscription.save()
        rxu.email_redeem_points()
        rxu.save()

        # TODO: Error handling
        return Response({'code': rxu.code,
                         'points_pending': subscription.points_pending,
                         'points_redeemed': subscription.points_redeemed,
                         'stock': reward.stock,
                         'should_share': True
                         })


    @detail_route(methods=['delete'])
    def unsubscribe(self, request, pk):
        #BUSCAR company=self.get_object() en self.request.user.subscriptions
        # REMOVE elementos en self.request.user.subscriptions
        self.request.user.subscriptions.filter(company=self.get_object()).delete()
        return Response({'status': 'OK'})

    #TODO: Que hace esto aca???
    @list_route(methods=['post'])
    def change_password(self, request):
        self.request.user.set_password(request.data.get('new_password'))
        self.request.user.save()
        return Response({'status': 'OK'})


class SocialAccountViewSet(viewsets.ModelViewSet):
    queryset = SocialAccount.objects.all()
    serializer_class = SocialAccountSerializer
    ordering_fields = ('email',)
    ordering = ('email',)
    search_fields = ('^email',)

    @list_route(methods=['put'])
    def add(self, request):
        email = request.data.get('email')
        uid = request.data.get('uid')
        social = request.data.get('provider')
        SocialAccount.objects.create(user=self.request.user, email=email, uid=uid, social=social)
        return Response({'status': 'OK'})


class RewardViewSet(viewsets.ModelViewSet):
    #TODO: Falta el filtro por usuario
    queryset = Reward.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    serializer_class = RewardSerializer
    parser_classes = (FormParser, MultiPartParser,)

    @list_route(methods=['GET'])
    def available_rewards(self, request):
        subscriptions = self.request.user.subscriptions.all()
        response = {'status': 'Ko'}
        for subscription in subscriptions:
            available_rewards = subscription.available_rewards(False)
            if available_rewards:
                response = {'status': 'Ok'}
        return Response(response)

    @list_route(methods=['GET'])
    def all_rewards(self, request):
        rewards = Reward.objects.all().order_by('points')
        serializer = RewardSerializer(rewards, many=True)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def get_new_rewards(self, request):
        subscriptions = self.request.user.subscriptions.all()
        rewards = []
        for subscription in subscriptions:
            available_rewards = subscription.available_rewards(True)
            if available_rewards:
                rewards.extend(available_rewards)
        if rewards:
            serializer = RewardSerializer(rewards, many=True)
            return Response(serializer.data)
        return Response({'status': 'OK'})

    @detail_route(methods=['post','put'])
    def redeem_points(self, request, pk):
        # Crear registro con codigo del premio a canjear. Estado a canjear.
        reward = self.get_object()
        company = reward.company
        # Bajar del stock en 1.
        reward.stock = reward.stock - 1
        reward.save()
        millis = int(round(time.time() * 1000))
        code = hashlib.sha224(str(millis) + '_' + str(self.request.user) + '_' + str(reward.pk)).hexdigest()[:6]
        rxu = RewardxUser(reward=reward, user=self.request.user, code=code, status='PENDIENTE')

        subscription = self.request.user.subscriptions.filter(company=company)[0]
        subscription.points_pending -= reward.points
        subscription.points_redeemed += reward.points
        subscription.save()
        rxu.email_redeem_points()
        rxu.save()

        # TODO: Error handling
        return Response({'code': rxu.code,
                         'points_pending': subscription.points_pending,
                         'points_redeemed': subscription.points_redeemed,
                         'stock': reward.stock,
                         'should_share': True
                         })

    @list_route(methods=['put'])
    def add(self, request):
        company = request.data.get('company')
        company = Company.objects.get(pk=company)
        name = request.data.get('name')
        description = request.data.get('description')
        type = request.data.get('type')
        terms = request.data.get('terms')
        points = request.data.get('points')
        period_from = request.data.get('period_from')
        period_to = request.data.get('period_to')
        active = request.data.get('active')
        image_url = 'www.mypoints.com.ar'
        image = request.FILES.get('file', None)
        stock = request.data.get('stock')
        Reward.objects.create(company=company, name=name, description=description, type=type, terms=terms, points=points,
                              period_from=period_from, period_to=period_to, active=active,
                              image_url=image_url, stock=stock, image=image)
        return Response({'status': 'OK'})

    @detail_route(methods=['POST'])
    def update(self, request, pk=None):
        reward = self.get_object()
        reward.name = request.data.get('name')
        reward.description = request.data.get('description')
        reward.type = request.data.get('type')
        reward.terms = request.data.get('terms')
        reward.points = request.data.get('points')
        reward.period_from = request.data.get('period_from')
        reward.period_to = request.data.get('period_to')
        reward.active = request.data.get('active')
        reward.image_url = request.data.get('image_url')
        reward.stock = request.data.get('stock')
        reward.image = request.FILES.get('file', None)
        reward.save()
        return Response({'status': 'OK'})

    @detail_route(methods=['delete'])
    def delete(self, request, pk):
        reward = self.get_object()
        reward.delete()
        return Response({'status': 'OK'})

    @detail_route(methods=['PUT'])
    def share_reward(self, request, pk=None):
        reward = self.get_object()
        sub = self.request.user.subscriptions.filter(company=reward.company)[0]
        sub.points_pending += reward.company.share_reward_points
        sub.shared_rewards.add(reward)
        sub.save()
        return Response({'status': 'OK'})


class RewardsXUserCompanySerializer(RewardsXUserSerializer):
    reward = RewardSerializer()
    company = UserCompanySerializer(source='reward.company')
    user = EndUserSerializer()
    class Meta:
        model = RewardxUser
        fields = ('pk', 'reward', 'code', 'pending', 'company', 'status', 'user')


class RewardsXUserViewSet(viewsets.ModelViewSet):
    queryset = RewardxUser.objects.filter(pending=True)
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    serializer_class = RewardsXUserCompanySerializer

    @detail_route(methods=['post'])
    def commit(self, request, pk=None):
        rxu = self.get_object()
        # Obtener la suscripcion a la empresa y descontar los puntos
        company = rxu.reward.company
        subscription = rxu.user.subscriptions.filter(company=company)[0]
        subscription.points_redeemed+=rxu.reward.points
        subscription.points_pending-=rxu.reward.points
        subscription.save()
        rxu.status = 'CANJEADO'
        rxu.pending = False
        rxu.save()
        rxu.email_reward_exchange() # Email al usuario
        return Response({'status': 'OK'})

    def get_queryset(self):
        qs = super(RewardsXUserViewSet, self).get_queryset()
        if self.request.user.is_company:
            return qs.filter(reward__company=self.request.user.my_company())
        return qs


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    serializer_class = CategorySerializer
    ordering_fields = ('description',)
    ordering = ('description', )
    search_fields = ('^description',)


class SubscriptionCompanySerializer(serializers.ModelSerializer):
    company = UserCompanySerializer()
    user = serializers.SerializerMethodField()
    class Meta:
        model = Subscription
        fields = ('pk', 'points_redeemed', 'points_pending', 'company', 'user')

    def get_user(self, obj):
        serializer = CompanyUserSerializer(obj.mypointsuser_set.all(), many=True)
        return serializer.data


class SubscriptionViewSet(viewsets.ModelViewSet):
    queryset = Subscription.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    serializer_class = SubscriptionCompanySerializer
    ordering_fields = ('company',)
    ordering = ('company', )
    search_fields = ('^company',)

    def get_queryset(self):
        qs = super(SubscriptionViewSet, self).get_queryset()
        if self.request.user.is_company:
            return qs.filter(company=self.request.user.my_company())
        return qs

    @detail_route(methods=['post'])
    def update_points(self, request, pk=None):
        sub = self.get_object()
        sub.points_pending+=request.data.get('points_pending')
        sub.save()
        return Response({'status': 'OK'})


from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator


class FirebaseToken(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(FirebaseToken, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        from fcm_django.models import FCMDevice
        token_id = request.POST['token_id']
        if (FCMDevice.objects.filter(registration_id = token_id).filter(active=True).count() == 0):
            device = FCMDevice()
            device.registration_id = token_id
            device.type = 'android'
            device.save()
        return JsonResponse({'status': True})


class ActivateAccount(RedirectView):

    def get(self,request):
        from django.contrib.auth.tokens import default_token_generator
        uid = request.GET.get('uid')
        user = MyPointsUser.objects.get(pk=uid)
        token = request.GET.get('token')
        if default_token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            user.email_welcome()
            return HttpResponseRedirect('http://mypoints.com.ar/mobileapp/#/activation')
        return HttpResponseRedirect('http://mypoints.com.ar/mobileapp/')