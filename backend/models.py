from django.db import models
from django.contrib.auth.models import User, BaseUserManager
from address.models import AddressField
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db.models import signals
from django.dispatch import receiver
from authtools.models import AbstractEmailUser
from django.utils import timezone

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.tokens import default_token_generator
import urllib
from email.MIMEImage import MIMEImage
from datetime import datetime

import os

CURRENCIES = [('AR$', 'Peso Argentino'), ('US$', 'US Dollar'), ('EUR', 'Euro')]
REWARD_TYPE = [('DISCOUNT', 'Descuento'), ('VOUCHER', 'Voucher'), ('PHYSICAL', 'Fisico')]
REWARD_STATUS = [('PENDIENTE','pendiente'), ('CANCELADO','cancelado'), ('CANJEADO','canjeado'), ('VENCIDO','vencido')]



class UserManager(BaseUserManager):
    def create(self, email, password='', is_admin=False, **kwargs):
        email = self.normalize_email(email)
        user = self.model(email=email, is_admin=is_admin, is_active=True, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **kwargs):
        return self.create(email, password, False, **kwargs)

    def create_superuser(self, email, password, **kwargs):
        return self.create(email, password, True, **kwargs)


class Category(models.Model):
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = _("Category")

    def __unicode__(self):
        return self.description


class Company(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    category = models.ForeignKey(Category)
    address = AddressField()
    description = models.TextField(blank=True)
    website = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    cuit = models.CharField(max_length=30)
    razon_social = models.CharField(max_length=255)
    legales = models.TextField()
    image_url = models.URLField(blank=True)
    share_reward_points = models.IntegerField(default=0)
    share_company_points = models.IntegerField(default=0)
    image = models.FileField(blank=True, default='')

    class Meta:
        verbose_name = _("Company")
        # The plurality here is clearly incorrect (read: "Accounts"), but this is intentional
        # as the User <-> Account relationship is strictly One-to-One; the admin panel label
        # would otherwise be confusing.
        verbose_name_plural = _("Company")
        ordering = ('user',)

    def __unicode__(self):
        return self.name

    def available_rewards(self):
        return self.reward_set.filter(stock__gt=0)

    def all_subscriptions(self):
        return Subscription.objects.filter(company=self)

    def shared_rewards(self, user):
        return Subscription.objects.filter(company=self).filter(user=user).shared_rewards.all()

    def real_image_url(self):
        if self.image:
            return 'http://mypoints.com.ar' + self.image.url
        return 'http://mypoints.com.ar/media/Premio_dinero.jpg'

    def email_new_company_available(self):
        plaintext = get_template('account/email_new_company.html')
        htmly = get_template('account/email_new_company.html')
        d = Context({'email': self.user.email, 'company': self.reward.company.name})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Nueva empresa en la familia MyPoints!', text_content, 'MyPoints <noreply@mypoints.com.ar>', [self.user.email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        msg.mixed_subtype = 'related'
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_img = MIMEImage(fp.read())
        msg_img.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_img)
        msg.send()


class Reward(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=100, null=True)
    description = models.TextField(default='')
    type = models.CharField(max_length=100, choices=REWARD_TYPE, default='DISCOUNT')
    currency = models.CharField(max_length=100, choices=CURRENCIES, null=True)
    terms = models.TextField(default='')
    points = models.IntegerField(default=0)
    stock = models.IntegerField(default=0)
    period_from = models.DateTimeField(null=True)
    period_to = models.DateTimeField(null=True)
    active = models.BooleanField(default=False)
    codigo = models.CharField(max_length=50, default='', null=True)
    #Image url
    image_url = models.URLField(blank=True, null=True)
    image = models.FileField(blank=True, default='')


    def real_image_url(self):
        if self.image:
            return 'http://mypoints.com.ar'+self.image.url
        return 'http://mypoints.com.ar/media/Premio_dinero.jpg'


    def __str__(self):
        return self.name

    def company_name(self):
        return self.company.name

    def company_category(self):
        return self.company.category.description

    def company_image_url(self):
        return self.company.real_image_url()

    def was_shared(self, user):
        rxu = RewardxUser.objects.filter(reward=self).filter(user=user).filter(shared_in_social=True)
        return len(rxu) > 0

    def email_new_reward_available(self, email):
        plaintext = get_template('account/email_new_reward_available.html')
        htmly = get_template('account/email_new_reward_available.html')
        d = Context({'email': email, 'company': self.company.name, 'reward': self.name, 'points': self.points})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Nuevo premio disponible!', text_content,
                                     'MyPoints <noreply@mypoints.com.ar>', [email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        msg.mixed_subtype = 'related'
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_img = MIMEImage(fp.read())
        msg_img.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_img)
        msg.send()


class Subscription(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    points_redeemed = models.IntegerField(default=0)
    points_pending = models.IntegerField(default=0)
    share_social = models.BooleanField(default=False)
    company = models.ForeignKey(Company, related_name='user_subscriptions')
    shared_rewards = models.ManyToManyField(Reward)
    last_checkout_awards = models.DateTimeField(default=datetime.now())

    def __unicode__(self):
         if self.mypointsuser_set.all():
            return self.company.name + '-' + self.mypointsuser_set.all()[0].email
         return self.company.name

    def get_user_email(self):
        if len(self.mypointsuser_set.all())>0:
            return self.mypointsuser_set.all()[0].email
        return None


    def available_rewards(self, update):
        rewards = Reward.objects.filter(company=self.company).filter(created__gt=self.last_checkout_awards)
        if update:
            self.last_checkout_awards = datetime.now()
            self.save()
        return rewards

class MyPointsUser(AbstractEmailUser):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    email_is_verified = models.BooleanField(default=False)
    is_company = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False, editable=False)
    is_super_admin = models.BooleanField(default=False, editable=False)
    # Only for end user. TODO: Buscar otra forma de hacerlo
    #points_redeemed = models.IntegerField(default=0)
    #points_pending = models.IntegerField(default=0)
    subscriptions = models.ManyToManyField(Subscription)
    first_name = models.CharField(_('first_name'), max_length=255, null=True)
    last_name = models.CharField(_('last_name'), max_length=255, null=True)
    phone = models.CharField(_('phone'), max_length=255, null=True)
    address = models.CharField(_('address'), max_length=255, null=True)
    city = models.CharField(_('city'), max_length=255, null=True)
    country = models.CharField(_('country'), max_length=255, null=True)
    state = models.CharField(_('state'), max_length=255, null=True)

    offset = models.SmallIntegerField('timezone offset in minutes', default=0)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def profile(self):
        return self.profile

    def my_rewards(self):
        return RewardxUser.objects.filter(user=self)

    def my_company(self):
        if self.is_company:
            return Company.objects.filter(user=self)
        return None

    def email_activation(self):
        plaintext = get_template('account/email_activation.html')
        htmly = get_template('account/email_activation.html')
        d = Context({'email': self.email, 'activate_url': 'http://www.mypoints.com.ar/activate?uid='+str(self.pk)+'&token='+default_token_generator.make_token(self)})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Email de Activacion', text_content, 'MyPoints <noreply@mypoints.com.ar>' ,[self.email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        msg.mixed_subtype = 'related'
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_img = MIMEImage(fp.read())
        msg_img.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_img)
        msg.send()

    def email_welcome(self):
        plaintext = get_template('account/email_welcome.html')
        htmly = get_template('account/email_welcome.html')
        d = Context({'email': self.email})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Bienvenido!!', text_content, 'MyPoints <noreply@mypoints.com.ar>', [self.email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        msg.mixed_subtype = 'related'
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_img = MIMEImage(fp.read())
        msg_img.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_img)
        msg.send()


class SocialAccount(models.Model):
    user = models.ForeignKey(MyPointsUser, related_name='social_accounts')
    email = models.EmailField(null=True)
    uid = models.CharField(max_length=255, null=True, default='')
    social = models.CharField(max_length=255, null=True, default='')

class RewardxUser(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    reward = models.ForeignKey(Reward)
    user = models.ForeignKey(MyPointsUser, related_name='rewards')
    code = models.CharField(max_length=255, null=False)
    pending = models.BooleanField(default=True)
    status = models.CharField(max_length=50, choices=REWARD_STATUS, default='PENDIENTE')
    shared_in_social = models.BooleanField(default=False)

    def email_redeem_points(self):
        plaintext = get_template('account/email_reward.html')
        htmly = get_template('account/email_reward.html')
        d = Context({'email': self.user.email, 'company': self.reward.company.name, 'reward': self.reward.name,
                     'code': self.code, 'points': self.reward.points, 'due_to': self.reward.period_to})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Ya casi estas!', text_content, 'MyPoints <noreply@mypoints.com.ar>',
                                     [self.user.email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        # TODO: Metodo en utils
        msg.mixed_subtype = 'related'

        # logo
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_mime = MIMEImage(fp.read())
        msg_mime.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_mime)
        # premio
        f = 'premio.png'
        if self.reward.image_url:
            fp = urllib.urlopen(self.reward.image_url)
            msg_mime = MIMEImage(fp.read())
            msg_mime.add_header('Content-ID', '<{}>'.format(f))
            msg.attach(msg_mime)
        msg.send()

    def email_reward_exchange(self):
        plaintext = get_template('account/email_reward_exchange.html')
        htmly = get_template('account/email_reward_exchange.html')
        d = Context({'email': self.user.email, 'company': self.reward.company.name, 'reward': self.reward.name})
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives('Ya esta contigo!', text_content, 'MyPoints <noreply@mypoints.com.ar>', [self.user.email])
        msg.attach_alternative(html_content, "text/html")

        # TODO: HORROR! Quitar esto asi
        msg.mixed_subtype = 'related'
        f = 'logo.png'
        fp = urllib.urlopen('http://mypoints.com.ar/mobileapp/image/logo.png')
        msg_img = MIMEImage(fp.read())
        msg_img.add_header('Content-ID', '<{}>'.format(f))
        msg.attach(msg_img)
        msg.send()


@receiver(signals.post_save, sender=MyPointsUser)
def create_account(sender, created, instance, **kwargs):
    if created:
        if instance.is_company:
            account = Company(user=instance)
            account.save()
        instance.email_activation()


@receiver(signals.pre_save, sender=Reward)
def update_created(sender, instance, **kwargs):
    if instance.active:
        instance.created = datetime.now()


@receiver(signals.post_save, sender=Company)
def send_reward_notification(sender, created, instance, **kwargs):
    # Buscar suscripciones de la misma empresa donde points_pending > instance.points
    if created:
        users_active = MyPointsUser.objects.filter(is_company=False).filter(email_is_verified=True).filter(is_admin=False).filter(is_super_admin=False)
        for user in users_active:
            # email y metodo para generar mensaje en el reward
            instance.email_new_company_available(user)

@receiver(signals.post_save, sender=Reward)
def send_reward_notification(sender, created, instance, **kwargs):
    # Buscar suscripciones de la misma empresa donde points_pending > instance.points
    from fcm_django.models import FCMDevice
    devices = FCMDevice.objects.all()
    devices.send_message(title="Nuevo premio", body="Hay un nuevo premio disponible en "+instance.company.name+": "+instance.name)
    if created and instance.active:
        subscriptions = Subscription.objects.filter(company=instance.company).filter(points_pending__gte=instance.points)
        for subscription in subscriptions:
            # Buscar el usuario que corresponda
            user_email = subscription.get_user_email()
            if user_email:
                # email y metodo para generar mensaje en el reward
                instance.email_new_reward_available(user_email)