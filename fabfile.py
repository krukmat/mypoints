from fabric.api import (
    cd, run as _run, task, env, hide, sudo as _sudo)
from fabric.colors import red, green, blue, yellow
from fabric.operations import require as require_env
from fabric.contrib.files import exists
from contextlib import contextmanager
import fabtools
import os
from functools import wraps
import fabric_gunicorn_custom as gunicorn

env.debian_packages = "debian_packages.txt"
env.vagrant_packages = 'vagrant_packages.txt'

env.project_name = 'myPoints'

env.python = 'python'
env.virtualenv_path = os.path.join('~', 'venv')
env.project_path = os.path.join('/var/www/', env.project_name)
env.git_url = 'https://krukmat@bitbucket.org/mypoints.git'

env.gunicorn_wsgi_app = 'website.wsgi:application'
env.gunicorn_bind = '0.0.0.0:8000'
env.remote_workdir = os.path.join('/var/www/', env.project_name)
env.gunicorn_workers = '4'
env.gunicorn_timeout = '300'
env.gunicorn_worker_class = 'gevent'


@task
def run_script(script):
    with project():
        run('python %s' % script)

@task
def install_git_repository(branch=None):
    require_env('project_path', 'git_url')

    if not exists(env.project_path):
        if env.get('is_vagrant') and exists('/vagrant'):
            if not exists('/var/www'):
                run('sudo mkdir /var/www/')
            run('sudo ln -s /vagrant %s' % env.project_path)
            run('sudo chown www-data:www-data -R %s' % env.project_path)
        else:
            fabtools.require.directory(env.project_path)
            fabtools.git.clone(env.git_url, path=env.project_path)
            fabtools.git.pull(path=env.project_path)
            run('sudo chown www-data:www-data -R %s' % env.project_path)
    else:
        if not(env.get('is_vagrant') and exists('/vagrant')):
            if not branch:
                fabtools.git.pull(path=env.project_path, force=True)
                run('sudo chown www-data:www-data -R %s' % env.project_path)

    if branch:
        fabtools.git.checkout(env.project_path, branch=branch, force=True)
        fabtools.git.pull(path=env.project_path, force=True)


def expand_paths():
    env_attributes = ['project_path', 'virtualenv_path']

    require_env(*env_attributes)

    env.home_dir = fabtools.user.home_directory(env.user)
    for name in env_attributes:
        setattr(env, name, getattr(env, name).replace('~', env.home_dir))

# TODO: No anda ni local ni server :(
@contextmanager
def project():
    """Runs commands within the project's directory."""
    require_env('project_path', 'virtualenv_path')

    with fabtools.python.virtualenv(env.virtualenv_path):
        with cd(env.project_path):
            yield


def _print(output):
    print os.linesep, output, os.linesep


def print_command(command):
    _print(blue("$ ", bold=True) +
           yellow(command, bold=True) +
           red(" ->", bold=True))


def run(command, show=True, args=("running",), quiet=False, shell=True):
    """Runs a shell comand on the remote server."""

    if show:
        print_command(command)
    else:
        args += ('stdout',)

    with hide(*args):
        return _run(command, quiet=quiet, shell=True)

@task
def sudo(command, show=True, shell_escape=None, shell=True):
    """Runs a command as sudo on the remote server."""
    if show:
        print_command(command)
    with hide("running"):
        return _sudo(command, shell_escape=shell_escape, shell=shell)


@task
def install_nginx_push_stream():
    sudo('apt-get build-dep nginx')
    run('apt-get source nginx')
    with cd('nginx-1.4.6/debian/modules'):
        run('curl -L https://github.com/wandenberg/nginx-push-stream-module/tarball/master | sudo tar zx')
        run('mv wandenberg-nginx-push-stream-module-9cc9f7f nginx-push-stream-module')
    with cd('nginx-1.4.6'):
        #run('dpkg-buildpackage -b')
        run('./configure --prefix=/usr/share/nginx --conf-path=/etc/nginx/nginx.conf --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --lock-path=/var/lock/nginx.lock --pid-path=/run/nginx.pid --http-client-body-temp-path=/var/lib/nginx/body --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --http-proxy-temp-path=/var/lib/nginx/proxy --http-scgi-temp-path=/var/lib/nginx/scgi --http-uwsgi-temp-path=/var/lib/nginx/uwsgi --with-debug --with-pcre-jit --with-ipv6 --with-http_ssl_module --with-http_stub_status_module --with-http_realip_module --with-http_addition_module --with-http_dav_module --with-http_geoip_module --with-http_gzip_static_module --with-http_image_filter_module --with-http_spdy_module --with-http_sub_module --with-http_xslt_module --with-mail --with-mail_ssl_module --with-poll_module --add-module=/home/vagrant/nginx-1.4.6/debian/modules/nginx-push-stream-module')
        run('make')
        sudo('sudo make install')
    sudo('mkdir /usr/share/nginx/logs/')
    sudo('ln -s --force /usr/share/nginx/sbin/nginx /usr/sbin/nginx')
    sudo('export NGINX_PUSH_STREAM_MODULE_PATH=/home/vagrant/nginx-1.4.6/debian/modules/nginx-push-stream-module/;nginx -c $NGINX_PUSH_STREAM_MODULE_PATH/misc/nginx.conf -t')
    sudo('export NGINX_PUSH_STREAM_MODULE_PATH=/home/vagrant/nginx-1.4.6/debian/modules/nginx-push-stream-module/;cp $NGINX_PUSH_STREAM_MODULE_PATH/misc/nginx.conf /etc/nginx/nginx.conf')
    #sudo('dpkg -i nginx-common_1.4.6-1ubuntu3.5_all.deb nginx-full_1.4.6-1ubuntu3.5_amd64.deb '
    #     'nginx_1.4.6-1ubuntu3.5_all.deb')


@task
def install_virtualenv():
    """
    Create a new virtual environment for a project.
    """

    require_env('virtualenv_path', 'project_path')

    # Replaces fabtools version since ther is some bug with pip install script
    # looks like curl fails on redirect.
    fabtools.deb.install('python-pip')
#     fabtools.python.install_pip()

    fabtools.python.install('virtualenv', use_sudo=True)

    if exists(env.virtualenv_path):
        run('rm -r %s' % env.virtualenv_path)

    # Create virtualenv
    run('virtualenv --no-site-packages -p %s --distribute %s' %
        (env.python, env.virtualenv_path))

    if not exists('~/activate'):
        run('ln -s %s/bin/activate ~/activate' % env.virtualenv_path)

        with cd(env.virtualenv_path):
            append_extract = 'export PYTHONPATH=%s' % env.project_path
            sudo('echo "%s" >> bin/activate' % append_extract)

    # Make shortcut
    sudo('ln -sf %s %s' % (env.virtualenv_path, os.path.join(env.project_path, 'venv')))

    _print(green('virtualenv installed'))

@task
def install_debian_packages():
    """Installation of debian packages"""
    require_env('debian_packages')
    require_env('vagrant_packages')

    if env.get('is_vagrant') and exists('/vagrant'):
        packages_file = env.vagrant_packages
    else:
        packages_file = env.debian_packages
    fabtools.deb.update_index()

    filename = os.path.join(os.path.dirname(__file__),
                            packages_file)

    with open(filename, 'r') as file_:
        packages = [line.strip() for line in file_]
    fabtools.deb.update_index()
    fabtools.deb.install(packages)
    fabtools.deb.upgrade()

@task
def install_python_modules():
    """Installation of python modules in requirements.txt"""
    with project():
        fabtools.python.install_requirements('requirements.txt')


def chmod(path, mode):
    sudo('chmod %s -R %s' % (mode, path))

@task
def configure_nginx():
    """Configuration of nginx module"""
    fabtools.require.nginx.server()
    if env['env_name'] == 'vagrant':
        fabtools.require.nginx.disable('default')
    django_kwargs = dict(
        user=env.user,
        server=env.app_host,
        project_path=env.project_path
    )
    with cd(env.project_path):
        sudo('chown www-data:www-data /var/www/%s' % env.project_name)
        # Split nginx script for vagrant from server.
        if env['env_name'] == 'vagrant':
            nginx_script = "ngnix/nginx.conf"
        else:
            nginx_script = "ngnix/nginx_server.conf"
        with open(nginx_script) as template_file:
            template_content = template_file.read()
            fabtools.require.nginx.site(env.app_host, template_content, **django_kwargs)
    fabtools.require.nginx.enable(env.app_host)

@task
def restart_nginx():
    fabtools.require.nginx.reload_service('nginx')

@task
def django_manage(command, *args, **kwargs):
    with project():
        str_args = ' '.join(args)
        str_kwargs = ' '.join(['%s=%s' % (k, v) for k, v in kwargs.items()])
        run('python manage.py %s %s %s' %
            (command, str_args, str_kwargs))

@task
def migrate_database():
    django_manage('migrate')

@task
def make_migrate_database():
    django_manage('makemigrations')


def environment(func):
    @wraps(func)
    def update_env(*args, **kawrgs):
        env['is_%s' % func.__name__] = True
        return func(*args, **kawrgs)
    return update_env


@task
@environment
def vagrant(name=''):
    """vagrant local"""
    from fabtools.vagrant import vagrant as _vagrant
    _vagrant(name)
    env.is_vagrant = True
    env.env_name = 'vagrant'
    env.user = 'vagrant'
    env.project_path = os.path.join('/var/www/', env.project_name)
    env.env_name = 'vagrant'
    env.app_host = 'mypoints.dev'
    env.remote_workdir = os.path.join('/var/www/', env.project_name)
    expand_paths()


@task
@environment
def server():
    """Server"""
    env.env_name = 'server'
    env.is_vagrant = False
    env.user = 'root'
    env.password = 'GyQPMELdA9f4'
    env.project_path = os.path.join('/var/www/', env.project_name)
    env.virtualenv_path = os.path.join('/root/')
    env.remote_workdir = os.path.join('/var/www/', env.project_name)
    env.hosts = ['mypoints.com.ar']
    env.app_host = 'mypoints.com.ar'
    env.port = '5580'

@task
def start_gunicorn():
    with fabtools.python.virtualenv(env.virtualenv_path):
        gunicorn.start()

@task
def restart_gunicorn():
    with fabtools.python.virtualenv(env.virtualenv_path):
        stop_gunicorn()
        start_gunicorn()

@task
def stop_gunicorn():
    with fabtools.python.virtualenv(env.virtualenv_path):
        run('kill `cat %s`' % (env.gunicorn_pidpath), quiet=True)
        run('sudo rm %s' % (env.gunicorn_pidpath), quiet=True)

@task
def update_angular_app():
    """ Install npm, gulp and bower """
    run('sudo npm install -g gulp@v3.9.0 bower')
    run('sudo ln -s /usr/bin/nodejs /usr/bin/node', quiet=True)
    with cd(env.project_path):
        with cd('./angularApp'):
            run('npm install')
            if env['env_name'] != 'vagrant':
                run('bower install --allow-root')
            else:
                run('bower install')
            # TODO: Error in step build for server env
            run('export DISABLE_NOTIFIER=true;gulp build --dev')

@task
def build_angular_app():
    install_git_repository()
    with cd(env.project_path):
        with cd('./angularApp'):
            run('export DISABLE_NOTIFIER=true;gulp build --dev')


@task
def update_mobile_app():
    """ Install npm, gulp and bower """
    #run('sudo npm install -g gulp@v3.9.0 bower')
    #run('sudo ln -s /usr/bin/nodejs /usr/bin/node', quiet=True)
    with cd(env.project_path):
        with cd('./mobile_platform/angularApp'):
            #run('npm install')
            #if env['env_name'] != 'vagrant':
            #    run('bower install --allow-root')
            #else:
            #    run('bower install')
            # TODO: Error in step build for server env
            if env['env_name'] != 'vagrant':
                run('export DISABLE_NOTIFIER=true;gulp build --dev')
            else:
                # TODO: error: tiene que correrse 2 veces para que se actualice...
                run('gulp build --local')


@task
def install_wrk_benchmark():
    fabtools.git.clone('https://github.com/wg/wrk.git', path="/".join([env.project_path, 'wrk']))
    with cd(env.project_path):
        with cd('wrk'):
            run('make')

@task
def run_wrk_benchmark():
    with cd(env.project_path):
        with cd('wrk'):
            run('./wrk -t12 -c400 -d30s http://127.0.0.1/django/')

@task
def collect_static():
    django_manage('collectstatic')

@task
def install():
    """
    Installs the base system and Python requirements for the entire server.
    """
    install_git_repository()

    install_debian_packages()

    install_virtualenv()

    install_python_modules()

    # TODO: No funca install_nginx_push_stream()

    configure_nginx()

    update_angular_app()

    update_mobile_app()

    migrate_database()

    collect_static()

    start_gunicorn()

@task
def shell():
    django_manage('shell')

@task
def update(): #TODO: En server se traba
    """Updates the git, angular, and model (if needed)"""
    install_git_repository()
    install_python_modules()
    update_mobile_app()
    configure_nginx() # TODO: Server NO anda
    migrate_database()
    stop_gunicorn()
    start_gunicorn()# TODO: Server NO anda


@task
def update_mobile():
    install_git_repository()
    update_mobile_app()


@task
def update_company_admin():
    install_git_repository()
    update_angular_app()
