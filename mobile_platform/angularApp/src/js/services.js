angular.module('angularApp.services', ['angularApp.config', 'ngResource', 'ui.bootstrap', 'ngToast'])
.factory('AuthService', function($window, $http, backend){
    return {
      login: function(user){
          return $http.post(backend + '/auth/login/', user);
      },
      register: function(user) {
          return $http.post(backend + '/auth/register/', user)
      },
      me: function() {
          return $http({url: backend + '/auth/me/', method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      social_account: function(social) {
          return $http({url: backend + '/social_accounts/add',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: social})
      },
      update_me: function(profile){
          return $http({url: backend + '/auth/me/',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: profile})
      },
      update_password: function(passwords){
          return $http({url: backend + '/companies/change_password',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: passwords})
      },
      available_rewards: function(){
        return $http({url: backend + '/rewards/available_rewards',  method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      }
    }
})
.factory('SubscriptionService', function($window, $http, backend){
  return {
    subscribe: function(company) {
          return $http({url: backend + '/companies/' + company + '/subscribe', method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
    },
    unsubscribe: function(company) {
          return $http({url: backend + '/companies/' + company + '/unsubscribe', method: 'DELETE', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
    },
    share_company_points: function(company){
        return $http({url: backend + '/companies/' + company + '/share_company_points', method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
    }
  }
})
.factory('RewardService', function($window, $http, backend){
    return {
        share: function(reward){
            return $http({url: backend + '/rewards/' + reward.pk + '/share_reward', method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
        }
    }
})
.factory('PointsService', function($window, $http, $http, backend){
return {
      redeem: function(company, itemId) {
          return $http({url: backend + '/companies/' + company + '/redeem_points',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: {item: itemId}})
      }
};
})
.factory('CategoryService', function($window, $http, $resource, backend){
  return $resource(backend + '/categories', {}, {
    get : {
      method : 'GET',
      headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }
    },
  });
})
.factory('CompanyService', function($window, $resource, backend){
  return $resource(backend + '/companies/:id', { id: '@_id' }, {
    get : {
      method : 'GET',
      headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }
    },
  });
})
.factory('CompanySearch', function($window, $resource, backend, $http){
  return {
      search: function(filters) {
        return $resource(backend + '/companies/', filters, {
          get : {
            method : 'GET',
            headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }
          }
        });
      }
  };
}).factory('RewardsNotificationCenter', function($http, $timeout, AuthService, ngToast, $window) {
  var data = { response: {}, calls: 0 };
  var poller = function() {
    if ($window.sessionStorage.token){
      AuthService.available_rewards().then(function(r) {
        data.response = r.data;
        data.calls++;
        console.log(data.response);
        var premios = '';
        // Llamar a un Toast. Ver como mostrarlo una vez y si el usuario cierra el toast no mostrarlo mas.
        for (var reward in data.response) {
          // Mostrar las novedades solo una vez
          if (window.rewards_available.indexOf(data.response[reward].pk)<0){
              premios+=data.response[reward].description+'<BR/>';
              window.rewards_available.push(data.response[reward].pk);
              if (window.rewards_available.indexOf(data.response[reward].company)<0)
                window.company_new_rewards.push(data.response[reward].company);
          }
        }
        });
      }
      $timeout(poller, 10000);
  };
  window.rewards_available = [];
  window.company_new_rewards = [];
  poller();

  return {
    data: data
  };
});
angular.module('angularApp.services').service('modalService', ['$modal',
    function ($modal) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

    }]);

